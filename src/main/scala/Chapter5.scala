package com.craigtreptow.scrayz

import Builders.{point, vector}
import RayUtils._
import MatrixUtils._
import CanvasUtils.{canvasToPPM, writePixelAt, canvasToLines, ppmBody}

object Chapter5:
  def castCircle(fileName: String) =
    val canvasPixels = 500
    var canvas = Canvas(width = canvasPixels, height = canvasPixels)
    val red = Color(red = 1, green = 0, blue = 0)
    val shape = Sphere()

    // start the ray at z = -5
    val rayOrigin = point(0, 0, -5)
    // put the wall at z = 10
    val wallZ = 10
    // following the ray to the wall gives +/- 3, so use 7 for a margin
    val wallSize = 7.0
    // divide wall size by number of pixels to get size of single pixel (in world space units)
    val pixelSize = wallSize / canvasPixels
    // assume you're looking directly at the center of the sphere
    // half of the wall is to the left and half is to the right
    // since the wall is centered around the origin, so
    // 'half' describes the min and max x and y coordinates of the wall
    val half = wallSize / 2

    // # for each row of pixels in the canvas​
    for y <- 0 to canvasPixels - 1 do
      // ​compute the world y coordinate (top = +half, bottom = -half)​
      val worldY = half - pixelSize * y

      // for each pixel in the row
      for x <- 0 to canvasPixels - 1 do
        // compute the world x coordinate (left = -half, right = half)​
        val worldX = -half + pixelSize * x

        // describe the point on the wall that the ray will target​
        val position = point(worldX, worldY, wallZ)

        val r = Ray(rayOrigin, (position - rayOrigin).normalize)
        val xs = intersect(shape, r)
        if xs.nonEmpty then canvas = writePixelAt(red, x, y, canvas)

    canvasToPPM(canvas, fileName)
