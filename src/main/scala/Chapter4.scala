package com.craigtreptow.scrayz

import Builders.{point, vector}
import MatrixUtils.{rotationY}
import CanvasUtils.{canvasToPPM, writePixelAt, canvasToLines, ppmBody}

object Chapter4:
  def plotClockFace(fileName: String) =
    val size = 500
    val halfSize = size / 2
    val blankCanvas = Canvas(width = size, height = size)
    val origin = point(halfSize, halfSize, halfSize)
    val red = Color(red = 1, green = 0, blue = 0)
    val green = Color(red = 0, green = 1, blue = 0)
    val radius = (3.0 / 8.0) * size
    val computedCoordinates = for hour <- 1 to 12 yield computeXY(hour, size, origin, radius)
    val newCanvas = plotCoordinates(computedCoordinates.toVector, blankCanvas, red)

    canvasToPPM(newCanvas, fileName)

  private def computeXY(hour: Int, size: Int, origin: Tuple, radius: Double): Tuple2[Int, Int] =
    val rotation = computeRotation(hour)
    val point = computePoint(rotation, origin, radius)
    (point.x.round.toInt, size - point.z.round.toInt)

  private def plotCoordinates(coordinates: Vector[Tuple2[Int, Int]], oldCanvas: Canvas, color: Color): Canvas =
    if coordinates.isEmpty then oldCanvas
    else
      val newCanvas =
        writePixelAt(newColor = color, x = coordinates.head._1, y = coordinates.head._2, canvas = oldCanvas)
      plotCoordinates(coordinates.tail, newCanvas, color)

  private def computePoint(rotation: Matrix, origin: Tuple, radius: Double): Tuple =
    val twelve = point(0, 0, 1)
    val rotatedTwelve = rotation * twelve
    val radiusAppliedPoint = applyRadius(rotatedTwelve, radius)
    val centeredPoint = moveToCenter(radiusAppliedPoint, origin)
    centeredPoint

  private def moveToCenter(pointToMove: Tuple, origin: Tuple): Tuple =
    point(pointToMove.x + origin.x, pointToMove.y + origin.y, pointToMove.z + origin.z)

  private def applyRadius(pointNeedsRadiusApplied: Tuple, radius: Double): Tuple =
    val x = pointNeedsRadiusApplied.x
    val z = pointNeedsRadiusApplied.z
    val xr = pointNeedsRadiusApplied.x * radius
    val zr = pointNeedsRadiusApplied.z * radius
    point(pointNeedsRadiusApplied.x * radius, pointNeedsRadiusApplied.y, pointNeedsRadiusApplied.z * radius)

  private def computeRotation(hour: Int): Matrix = rotationY(radians = hour * (Math.PI / 6))
