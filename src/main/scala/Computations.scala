package com.craigtreptow.scrayz

case class Computations(
    val calculatedtValue: Double,
    val calculatedObject: Sphere,
    val calculatedPoint: Tuple,
    val calculatedEyeVector: Tuple,
    val calculatedNormalVector: Tuple,
    val inside: Boolean,
    val overPoint: Tuple
)
