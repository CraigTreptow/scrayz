package com.craigtreptow.scrayz

case class Ray(val origin: Tuple, val direction: Tuple)
