package com.craigtreptow.scrayz

import com.craigtreptow.scrayz.{Utils => U}

case class Color(val red: Double, val green: Double, val blue: Double):
  def ==(that: Color): Boolean =
    val redResult = U.equals(this.red, that.red)
    val greenResult = U.equals(this.green, that.green)
    val blueResult = U.equals(this.blue, that.blue)
    val finalResult = redResult && greenResult && blueResult
    finalResult

  def +(that: Color): Color =
    Color(
      red = this.red + that.red,
      green = this.green + that.green,
      blue = this.blue + that.blue
    )

  def -(that: Color): Color =
    Color(
      red = this.red - that.red,
      green = this.green - that.green,
      blue = this.blue - that.blue
    )

  def *(that: Double): Color =
    Color(
      red = this.red * that,
      green = this.green * that,
      blue = this.blue * that
    )

  def *(that: Color): Color =
    Color(
      red = this.red * that.red,
      green = this.green * that.green,
      blue = this.blue * that.blue
    )
