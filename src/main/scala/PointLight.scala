package com.craigtreptow.scrayz

case class PointLight(val position: Tuple, val intensity: Color)
