package com.craigtreptow.scrayz

import RayUtils._
import scala.compiletime.ops.boolean

object MaterialUtils:
  def lighting(material: Material, light: PointLight, point: Tuple, eyeV: Tuple, normalV: Tuple, shadowed: Boolean = false): Color =
    val black = Color(0, 0, 0)

    // combine the surface color with the light's color/intensity​
    val effectiveColor = material.color * light.intensity

    // ​find the direction to the light source​
    val lightV = (light.position - point).normalize

    // ​compute the ambient contribution​
    val ambient = effectiveColor * material.ambient

    // light_dot_normal represents the cosine of the angle between the​
    // ​light vector and the normal vector. A negative number means the​
    // ​light is on the other side of the surface.​
    val lightDotNormal = lightV dot normalV

    val diffuse =
      if lightDotNormal < 0 then black
      else
        // compute the diffuse contribution​
        effectiveColor * material.diffuse * lightDotNormal

    val specular =
      if lightDotNormal < 0 then black
      else
        // reflect_dot_eye represents the cosine of the angle between the​
        // reflection vector and the eye vector. A negative number means the​
        // light reflects away from the eye.​
        val reflectV = reflect(lightV.negate, normalV)
        val reflectDotEye = reflectV dot eyeV

        if reflectDotEye <= 0 then black
        else
          // compute the specular contribution​
          val factor = Math.pow(reflectDotEye, material.shininess)
          light.intensity * material.specular * factor

    if shadowed then ambient else ambient + diffuse + specular
