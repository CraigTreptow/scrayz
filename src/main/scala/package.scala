package com.craigtreptow

package object scrayz:
  type CanvasGridType = Vector[Color]
  type MatrixType = Array[Array[Double]]

  val DefaultColor = Color(red = 0, green = 0, blue = 0)

  val EPSILON = 0.00001
