package com.craigtreptow.scrayz

import MatrixUtils.{scaling}

object Builders:
  def point(x: Double, y: Double, z: Double): Tuple = Tuple(x = x, y = y, z = z, w = 1.0)

  def vector(x: Double, y: Double, z: Double): Tuple = Tuple(x = x, y = y, z = z, w = 0)

  def defaultWorld() =
    val lightColor = Color(1, 1, 1)
    val lightPosition = point(-10, 10, -10)
    val pl = PointLight(position = lightPosition, intensity = lightColor)
    val c = Color(0.8, 1.0, 0.6)
    val m = Material(color = c, diffuse = 0.7, specular = 0.2)
    val s1 = Sphere(material = m)
    val t = scaling(0.5, 0.5, 0.5)
    val s2 = Sphere(transform = t)

    World(objects = List(s1, s2), light = pl)
