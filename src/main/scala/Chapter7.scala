package com.craigtreptow.scrayz

import Builders.{point, vector}
import com.craigtreptow.scrayz.{RayUtils => RU}
import MatrixUtils._
import MaterialUtils._
import Utils.{render}
import CanvasUtils.{canvasToPPM, writePixelAt, canvasToLines, ppmBody}

object Chapter7:
  def renderScene(fileName: String) =
    val horizontalPixels = 500
    val verticalPixels = 250

    val floorTransform = scaling(10, 0.01, 10)
    val darkTan = Color(1, 0.9, 0.9)
    val floorMaterial = Material(color = darkTan, specular = 0)
    val floor = Sphere(material = floorMaterial, transform = floorTransform)

    val piOverTwo = Math.PI / 2
    val piOverFour = Math.PI / 4

    val leftWallTransform =
      translation(0, 0, 5) * rotationY(-piOverFour) * rotationX(piOverTwo) * scaling(10, 0.01, 10)
    val leftWall = Sphere(material = floorMaterial, transform = leftWallTransform)

    val rightWallTransform =
      translation(0, 0, 5) * rotationY(piOverFour) * rotationX(piOverTwo) * scaling(10, 0.01, 10)
    val rightWall = Sphere(material = floorMaterial, transform = rightWallTransform)

    val greenish = Color(0.1, 1, 0.5)
    val middleTransform = translation(-0.5, 1, 0.5)
    val middleMaterial = Material(color = greenish, diffuse = 0.7, specular = 0.3)
    val middleSphere = Sphere(material = middleMaterial, transform = middleTransform)

    val rightTransform = translation(1.5, 0.5, -0.5) * scaling(0.5, 0.5, 0.5)
    val rightMaterial = Material(color = greenish, diffuse = 0.7, specular = 0.3)
    val rightSphere = Sphere(material = middleMaterial, transform = rightTransform)

    val brownish = Color(1, 0.8, 0.1)
    val leftTransform = translation(-1.5, 0.33, -0.75) * scaling(0.33, 0.33, 0.33)
    val leftMaterial = Material(color = brownish, diffuse = 0.7, specular = 0.3)
    val leftSphere = Sphere(material = leftMaterial, transform = leftTransform)

    // white light shining from above and to the left
    val light = PointLight(position = point(-10, 10, -9), intensity = Color(0.7, 0.7, 0.7))

    val cameraTransform = viewTransform(from = point(0, 1.5, -5), to = point(0, 1, 0), up = point(0, 1, 0))
    val camera =
      Camera(
        hSize = horizontalPixels,
        vSize = verticalPixels,
        fieldOfView = Math.PI / 3.8,
        transform = cameraTransform
      )

    val worldObjects = List(floor, leftWall, rightWall, middleSphere, rightSphere, leftSphere)
    val world = World(objects = worldObjects, light = light)

    var canvas = render(camera, world)
    canvasToPPM(canvas, fileName)
