package com.craigtreptow.scrayz

import Builders.{point, vector}
import MatrixUtils._
import RayUtils._
import WorldUtils._

class IntersectionsFeature extends FeatureSpec {
  Scenario("An intersection encapsulates t and object") {
    Given("s ← sphere()")
    val s = Sphere()

    When("i ← intersection(3.5, s)")
    val i = Intersection(3.5, s)

    Then("i.t = 3.5")
    assert(i.tValue == 3.5)

    And("i.object = s")
    assert(i.intersectedObject == s)
  }

  Scenario("Aggregating intersections") {
    Given("s ← sphere()")
    val s = Sphere()

    And("i1 ← intersection(1, s)")
    val i1 = Intersection(1, s)

    And("i2 ← intersection(2, s)")
    val i2 = Intersection(2, s)

    When("xs ← intersections(i1, i2)")
    val xs = intersections(List(i1, i2))

    Then("xs.count = 2")
    assert(xs.length == 2)

    And("xs[0].t = 1")
    assert(xs(0).tValue == 1)

    And("xs[1].t = 2")
    assert(xs(1).tValue == 2)
  }

  Scenario("Intersect sets the object on the intersections") {
    Given("r ← ray(point(0, 0, 5), vector(0, 0, 1))")
    val origin = point(x = 0, y = 0, z = 5)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("s ← sphere()")
    val s = Sphere()

    When("xs ← intersect(s, r)")
    val xs = intersect(s, r)

    Then("xs.count = 2")
    assert(xs.length == 2)

    And("xs[0].object = s")
    assert(xs(0).intersectedObject == s)

    And("xs[1].object = s")
    assert(xs(1).intersectedObject == s)
  }

  Scenario("The hit, when all intersections have positive t") {
    Given("s ← sphere()")
    val s = Sphere()

    And("i1 ← intersection(1, s)")
    val i1 = Intersection(1, s)

    And("i2 ← intersection(2, s)")
    val i2 = Intersection(2, s)

    And("xs ← intersections(i2, i1)")
    val xs = intersections(List(i2, i1))

    When("i ← hit(xs)")
    val i = hit(xs)

    Then("i = i1")
    i match
      case Some(result) => assert(result == i1)
      case None         => assert(false == true)
  }

  Scenario("The hit, when some intersections have negative t") {
    Given("s ← sphere()")
    val s = Sphere()

    And("i1 ← intersection(-1, s)")
    val i1 = Intersection(-1, s)

    And("i2 ← intersection(1, s)")
    val i2 = Intersection(1, s)

    And("xs ← intersections(i2, i1)")
    val xs = intersections(List(i2, i1))

    When("i ← hit(xs)")
    val i = hit(xs)

    Then("i = i2")
    i match
      case Some(result) => assert(result == i2)
      case None         => assert(false == true)
  }

  Scenario("The hit, when all intersections have negative t") {
    Given("s ← sphere()")
    val s = Sphere()

    And("i1 ← intersection(-2, s)")
    val i1 = Intersection(-2, s)

    And("i2 ← intersection(-1, s)")
    val i2 = Intersection(-1, s)

    And("xs ← intersections(i2, i1)")
    val xs = intersections(List(i2, i1))

    When("i ← hit(xs)")
    val i = hit(xs)

    Then("i is nothing")
    assert(i == None)
  }

  Scenario("The hit is always the lowest nonnegative intersection") {
    Given("s ← sphere()")
    val s = Sphere()

    And("i1 ← intersection(5, s)")
    val i1 = Intersection(5, s)

    And("i2 ← intersection(7, s)")
    val i2 = Intersection(7, s)

    And("i3 ← intersection(-3, s)")
    val i3 = Intersection(-3, s)

    And("i4 ← intersection(2, s)")
    val i4 = Intersection(2, s)

    And("xs ← intersections(i1, i2, i3, i4)")
    val xs = intersections(List(i1, i2, i3, i4))

    When("i ← hit(xs)")
    val i = hit(xs)

    Then("i = i4")
    i match
      case Some(result) => assert(result == i4)
      case None         => assert(false == true)
  }

  Scenario("Precomputing the state of an intersection") {
    Given("r ← ray(point(0, 0, -5), vector(0, 0, 1))")
    val origin = point(x = 0, y = 0, z = -5)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("shape ← sphere()")
    val shape = Sphere()

    And("i ← intersection(4, shape)")
    val i = Intersection(tValue = 4, intersectedObject = shape)

    When("comps ← prepare_computations(i, r)")
    val comps = prepareComputations(i, r)

    Then("comps.t = i.t")
    assert(comps.calculatedtValue == i.tValue)

    And("comps.object = i.object")
    assert(comps.calculatedObject == i.intersectedObject)

    And("comps.point = point(0, 0, -1)")
    assert(comps.calculatedPoint == point(0, 0, -1))

    And("comps.eyev = vector(0, 0, -1)")
    assert(comps.calculatedEyeVector == vector(0, 0, -1))

    And("comps.normalv = vector(0, 0, -1)")
    assert(comps.calculatedNormalVector == vector(0, 0, -1))
  }

  Scenario("The hit, when an intersection occurs on the outside") {
    Given("r ← ray(point(0, 0, -5), vector(0, 0, 1))")
    val origin = point(x = 0, y = 0, z = -5)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("shape ← sphere()")
    val shape = Sphere()

    And("i ← intersection(4, shape)")
    val i = Intersection(4, shape)

    When("comps ← prepareComputations(i, r)")
    val comps = prepareComputations(i, r)

    Then("comps.inside = false")
    assert(comps.inside == false)
  }

  Scenario("The hit, when an intersection occurs on the inside") {
    Given("r ← ray(point(0, 0, 0), vector(0, 0, 1))")
    val origin = point(x = 0, y = 0, z = 0)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("shape ← sphere()")
    val shape = Sphere()

    And("i ← intersection(1, shape)")
    val i = Intersection(1, shape)

    When("comps ← prepare_computations(i, r)")
    val comps = prepareComputations(i, r)

    Then("comps.point = point(0, 0, 1)")
    assert(comps.calculatedPoint == point(0, 0, 1))

    And("comps.eyev = vector(0, 0, -1)")
    assert(comps.calculatedEyeVector == point(0, 0, -1))

    And("comps.inside = true")
    assert(comps.inside == true)

    And("comps.normalv = vector(0, 0, -1)")
    // normal would have been (0, 0, 1), but is inverted!
    assert(comps.calculatedNormalVector == vector(0, 0, -1))
  }

  Scenario(" The hit should offset the point") {
    Given("r ← ray(point(0, 0, -5), vector(0, 0, 1))")
    val origin = point(x = 0, y = 0, z = -5)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("""shape ← sphere() with:
           | transform | translation(0, 0, 1) |
        """)
    val t = translation(0, 0, 1)
    val shape = Sphere(transform = t)

    And("i ← intersection(5, shape)")
    val i = Intersection(5, shape)

    When("comps ← prepare_computations(i, r)")
    val comps = prepareComputations(i, r)

    Then("comps.over_point.z < -EPSILON/2")
    assert(comps.overPoint.z < -(EPSILON / 2))

    And("comps.point.z > comps.over_point.z")
    assert(comps.calculatedPoint.z > comps.overPoint.z)
  }

//    Scenario: Precomputing the reflection vector
//      Given shape ← plane()
//    And r ← ray(point(0, 1, -1), vector(0, -√2/2, √2/2))
//    And i ← intersection(√2, shape)
//    When comps ← prepare_computations(i, r)
//    Then comps.reflectv = vector(0, √2/2, √2/2)
//
//
//    Scenario: The under point is offset below the surface
//      Given r ← ray(point(0, 0, -5), vector(0, 0, 1))
//    And shape ← glass_sphere() with:
//    | transform | translation(0, 0, 1) |
//      And i ← intersection(5, shape)
//    And xs ← intersections(i)
//    When comps ← prepare_computations(i, r, xs)
//    Then comps.under_point.z > EPSILON/2
//    And comps.point.z < comps.under_point.z
//
//
//    Scenario: The hit, when all intersections have negative t
//      Given s ← sphere()
//    And i1 ← intersection(-2, s)
//    And i2 ← intersection(-1, s)
//    And xs ← intersections(i2, i1)
//    When i ← hit(xs)
//    Then i is nothing
//
//    Scenario: The hit is always the lowest nonnegative intersection
//      Given s ← sphere()
//    And i1 ← intersection(5, s)
//    And i2 ← intersection(7, s)
//    And i3 ← intersection(-3, s)
//    And i4 ← intersection(2, s)
//    And xs ← intersections(i1, i2, i3, i4)
//    When i ← hit(xs)
//    Then i = i4
//
//    Scenario Outline: Finding n1 and n2 at various intersections
//    Given A ← glass_sphere() with:
//    | transform                 | scaling(2, 2, 2) |
//      | material.refractive_index | 1.5              |
//      And B ← glass_sphere() with:
//    | transform                 | translation(0, 0, -0.25) |
//      | material.refractive_index | 2.0                      |
//      And C ← glass_sphere() with:
//    | transform                 | translation(0, 0, 0.25) |
//      | material.refractive_index | 2.5                     |
//      And r ← ray(point(0, 0, -4), vector(0, 0, 1))
//    And xs ← intersections(2:A, 2.75:B, 3.25:C, 4.75:B, 5.25:C, 6:A)
//    When comps ← prepare_computations(xs[<index>], r, xs)
//    Then comps.n1 = <n1>
//      And comps.n2 = <n2>
//
//        Examples:
//        | index | n1  | n2  |
//        | 0     | 1.0 | 1.5 |
//        | 1     | 1.5 | 2.0 |
//        | 2     | 2.0 | 2.5 |
//        | 3     | 2.5 | 2.5 |
//        | 4     | 2.5 | 1.5 |
//        | 5     | 1.5 | 1.0 |
//
//        Scenario: The Schlick approximation under total internal reflection
//        Given shape ← glass_sphere()
//        And r ← ray(point(0, 0, √2/2), vector(0, 1, 0))
//        And xs ← intersections(-√2/2:shape, √2/2:shape)
//        When comps ← prepare_computations(xs[1], r, xs)
//        And reflectance ← schlick(comps)
//        Then reflectance = 1.0
//
//        Scenario: The Schlick approximation with a perpendicular viewing angle
//        Given shape ← glass_sphere()
//        And r ← ray(point(0, 0, 0), vector(0, 1, 0))
//        And xs ← intersections(-1:shape, 1:shape)
//        When comps ← prepare_computations(xs[1], r, xs)
//        And reflectance ← schlick(comps)
//        Then reflectance = 0.04
//
//        Scenario: The Schlick approximation with small angle and n2 > n1
//        Given shape ← glass_sphere()
//        And r ← ray(point(0, 0.99, -2), vector(0, 0, 1))
//        And xs ← intersections(1.8589:shape)
//        When comps ← prepare_computations(xs[0], r, xs)
//        And reflectance ← schlick(comps)
//        Then reflectance = 0.48873
//
//        Scenario: An intersection can encapsulate `u` and `v`
//        Given s ← triangle(point(0, 1, 0), point(-1, 0, 0), point(1, 0, 0))
//        When i ← intersection_with_uv(3.5, s, 0.2, 0.4)
//        Then i.u = 0.2
//        And i.v = 0.4

}
