package com.craigtreptow.scrayz

import Builders.{point}

class LightsFeature extends FeatureSpec {
  Scenario("A point light has a position and intensity") {
    Given("intensity ← color(1, 1, 1)")
    val intensity = Color(red = 1, green = 1, blue = 1)

    And("position ← point(0, 0, 0)")
    val position = point(0, 0, 0)

    When("light ← point_light(position, intensity)")
    val light = PointLight(position = position, intensity = intensity)

    Then("light.position = position")
    assert(light.position == position)

    Then("light.intensity = intensity")
    assert(light.intensity == intensity)
  }
}
