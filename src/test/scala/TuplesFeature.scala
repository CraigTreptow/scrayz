package com.craigtreptow.scrayz

import Builders.{point, vector}

class TuplesFeature extends FeatureSpec {
  Feature("Tuples") {
    Scenario("A tuple with w=1.0 is a point") {
      Given("a ← tuple(4.3, -4.2, 3.1, 1.0)")
      val expectedX = 4.3
      val expectedY = -4.2
      val expectedZ = 3.1
      val expectedW = 1.0
      val a = Tuple(x = expectedX, y = expectedY, z = expectedZ, w = expectedW)

      Then("a should have the correct attributes")
      assert(a.x == expectedX)
      assert(a.y == expectedY)
      assert(a.z == expectedZ)
      assert(a.w == expectedW)

      And("a should be a Point")
      assert(a.isPoint)

      And("a should not be a Vector")
      assert(!a.isVector)
    }

    Scenario("A tuple with w=0 is a vector") {
      Given("a ← tuple(4.3, -4.2, 3.1, 0.0)")
      val expectedX = 4.3
      val expectedY = -4.2
      val expectedZ = 3.1
      val expectedW = 0.0
      val a = Tuple(x = expectedX, y = expectedY, z = expectedZ, w = expectedW)

      Then("a should have the correct attributes")
      assert(a.x == expectedX)
      assert(a.y == expectedY)
      assert(a.z == expectedZ)
      assert(a.w == expectedW)

      And("a should not be a Point")
      assert(!a.isPoint)

      And("a should be a Vector")
      assert(a.isVector)
    }

    Scenario("Adding two tuples") {
      Given("a1 ← tuple(3, -2, 5, 1)")
      val a1 = Tuple(x = 3, y = -2, z = 5, w = 1)

      And("a2 ← tuple(-2, 3, 1, 0)")
      val a2 = Tuple(x = -2, y = 3, z = 1, w = 0)

      Then("a1 + a2 = tuple(1, 1, 6, 1)")
      val computed = a1 + a2
      val expected = Tuple(x = 1, y = 1, z = 6, w = 1)
      assert(computed == expected)
    }

    Scenario("Negating a tuple") {
      Given("a ← tuple(1, -2, 3, -4)")
      val a = Tuple(x = 1, y = -2, z = 3, w = -4)

      Then("-a = tuple(-1, 2, -3, 4)")
      val computed = a.negate
      val expected = Tuple(x = -1, y = 2, z = -3, w = 4)
      assert(computed == expected)
    }

    Scenario("Multiplying a tuple by a scalar") {
      Given("a ← tuple(1, -2, 3, -4)")
      val a = Tuple(x = 1, y = -2, z = 3, w = -4)

      Then("a * 3.5 = tuple(3.5, -7, 10.5, -14)")
      val computed = a * 3.5
      val expected = Tuple(x = 3.5, y = -7, z = 10.5, w = -14)
      assert(computed == expected)
    }

    Scenario("Multiplying a tuple by a fraction") {
      Given("a ← tuple(1, -2, 3, -4)")
      val a = Tuple(x = 1, y = -2, z = 3, w = -4)

      Then("a * 3.5 = tuple(0.5, -1, 1.5, -2)")
      val computed = a * 0.5
      val expected = Tuple(x = 0.5, y = -1, z = 1.5, w = -2)
      assert(computed == expected)
    }

    Scenario("Dividing a tuple by a scalar") {
      Given("a ← tuple(1, -2, 3, -4)")
      val a = Tuple(x = 1, y = -2, z = 3, w = -4)

      Then("a / 2 = tuple(0.5, -1, 1.5, -2)")
      val computed = a / 2
      val expected = Tuple(x = 0.5, y = -1, z = 1.5, w = -2)
      assert(computed == expected)
    }

    Scenario("the dot product of two tuples") {
      Given("a ← vector(1, 2, 3)")
      val a = vector(x = 1, y = 2, z = 3)

      Given("b ← vector(2, 3, 4)")
      val b = vector(x = 2, y = 3, z = 4)

      Then("dot(a, b) = 20")
      assert((a dot b) == 20)
    }
  }

  Feature("Points") {
    Scenario("Point() creates tuples with w=1") {
      Given("p ← point(4, -4, 3)")
      val expectedX = 4
      val expectedY = -4
      val expectedZ = 3
      val p = point(x = expectedX, y = expectedY, z = expectedZ)
      val t = Tuple(x = expectedX, y = expectedY, z = expectedZ, w = 1.0)

      Then("'p' = tuple(4, -4, 3, 1)")
      assert(p.w == 1.0)
      assert(t == p)
    }

    Scenario("Subtracting two points") {
      Given("p1 ← point(3, 2, 1)")
      val p1 = point(x = 3, y = 2, z = 1)

      And("And p2 ← point(5, 6, 7)")
      val p2 = point(x = 5, y = 6, z = 7)

      Then("p1 - p2 = vector(-2, -4, -6)")
      val computed = p1 - p2
      val expected = vector(x = -2, y = -4, z = -6)
      assert(computed == expected)
    }

    Scenario("Subtracting a vector from a point") {
      Given("p ← point(3, 2, 1)")
      val p = point(x = 3, y = 2, z = 1)

      And("And p2 ← point(5, 6, 7)")
      val v = vector(x = 5, y = 6, z = 7)

      Then("p1 - p2 = vector(-2, -4, -6)")
      val computed = p - v
      val expected = point(x = -2, y = -4, z = -6)
      assert(computed == expected)
    }
  }

  Feature("Vectors") {
    Scenario("Vector() creates tuples with w=0") {
      Given("v ← point(4, -4, 3)")
      val expectedX = 4
      val expectedY = -4
      val expectedZ = 3
      val v = vector(x = expectedX, y = expectedY, z = expectedZ)
      val t = Tuple(x = expectedX, y = expectedY, z = expectedZ, w = 0)

      Then("v = tuple(4, -4, 3, 0)")
      assert(v.w == 0)
      assert(t == v)
    }

    Scenario("Subtracting two vectors") {
      Given("v1 ← vector(3, 2, 1)")
      val v1 = vector(x = 3, y = 2, z = 1)

      Given("v2 ← vector(5, 6, 7)")
      val v2 = vector(x = 5, y = 6, z = 7)

      Then("v1 - v2 = vector(-2, -4, -6)")
      val computed = v1 - v2
      val expected = vector(x = -2, y = -4, z = -6)
      assert(computed == expected)
    }

    Scenario("Subtracting a vector from the zero vector") {
      Given("zero ← vector(0, 0, 0)")
      val zero = vector(x = 0, y = 0, z = 0)

      Given("v ← vector(1, -2, 3)")
      val v = vector(x = 1, y = -2, z = 3)

      Then("zero - v = vector(-1, 2, -3)")
      val computed = zero - v
      val expected = vector(x = -1, y = 2, z = -3)
      assert(computed == expected)
    }

    Scenario("Computing the magnitude of vector(1, 0, 0)") {
      Given("v ← vector(1, 0, 0)")
      val v = vector(x = 1, y = 0, z = 0)

      Then("magnitude(v) = 1")
      assert(v.magnitude == 1)
    }

    Scenario("Computing the magnitude of vector(0, 1, 0)") {
      Given("v ← vector(0, 1, 0)")
      val v = vector(x = 0, y = 1, z = 0)

      Then("magnitude(v) = 1")
      assert(v.magnitude == 1)
    }

    Scenario("Computing the magnitude of vector(0, 0, 1)") {
      Given("v ← vector(0, 0, 1)")
      val v = vector(x = 0, y = 0, z = 1)

      Then("magnitude(v) = 1")
      assert(v.magnitude == 1)
    }

    Scenario("Computing the magnitude of vector(1, 2, 3)") {
      Given("v ← vector(1, 2, 3)")
      val v = vector(x = 1, y = 2, z = 3)

      Then("magnitude(v) = √14")
      assert(v.magnitude == Math.sqrt(14))
    }

    Scenario("Computing the magnitude of vector(-1, -2, -3)") {
      Given("v ← vector(-1, -2, -3)")
      val v = vector(x = -1, y = -2, z = -3)

      Then("magnitude(v) = √14")
      assert(v.magnitude == Math.sqrt(14))
    }

    Scenario("Normalizing vector(4, 0, 0) gives (1, 0, 0)") {
      Given("v ← vector(4, 0, 0)")
      val v = vector(x = 4, y = 0, z = 0)

      Then("normalize(v) = vector(1, 0, 0)")
      val computed = v.normalize
      val expected = vector(x = 1, y = 0, z = 0)
      assert(computed == expected)
    }

    Scenario("Normalizing vector(1, 2, 3)") {
      Given("v ← vector(1, 2, 3)")
      val v = vector(x = 1, y = 2, z = 3)

      Then("Normalizing vector(1, 2, 3)")
      val computed = v.normalize

      val sqrtOfFourteen = Math.sqrt(14)
      val expectedX = 1 / sqrtOfFourteen
      val expectedY = 2 / sqrtOfFourteen
      val expectedZ = 3 / sqrtOfFourteen
      val expected = vector(x = expectedX, y = expectedY, z = expectedZ)
      assert(computed == expected)
    }

    Scenario("The magnitude of a normalized vector") {
      Given("v ← vector(1, 2, 3)")
      val v = vector(x = 1, y = 2, z = 3)

      When("norm ← normalize(v)")
      val norm = v.normalize

      Then("magnitude(norm) = 1")
      assert(norm.magnitude == 1)
    }

    Scenario("The cross product of two vectors") {
      Given("a ← vector(1, 2, 3)")
      val a = vector(x = 1, y = 2, z = 3)

      And("b ← vector(2, 3, 4)")
      val b = vector(x = 2, y = 3, z = 4)

      Then("cross(a, b) = vector(-1, 2, -1)")
      val computedAB = a cross b
      val expectedAB = vector(x = -1, y = 2, z = -1)
      assert(computedAB == expectedAB)

      And("cross(b, a) = vector(1, -2, 1)")
      val computedBA = b cross a
      val expectedBA = vector(x = 1, y = -2, z = 1)
      assert(computedBA == expectedBA)
    }
  }
}
