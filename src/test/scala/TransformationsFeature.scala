package com.craigtreptow.scrayz

import Builders.{point, vector}
import MatrixUtils._

import scala.math.{Pi, sqrt}

class TransformationsFeature extends FeatureSpec {
  Feature("Matrix Transformations") {
    Scenario("Multiplying by a translation matrix") {
      Given("transform ← translation(5, -3, 2)")
      val transform = translation(5, -3, 2)

      And("p ← point(-3, 4, 5)")
      val p = point(x = -3, y = 4, z = 5)

      Then("transform * p = point(2, 1, 7)")
      val expectedP = point(x = 2, y = 1, z = 7)
      assert(transform * p == expectedP)
    }

    Scenario("Multiplying by the inverse of a translation matrix") {
      Given("transform ← translation(5, -3, 2)")
      val transform = translation(5, -3, 2)

      And("inv ← inverse(transform)")
      val inv = inverse(transform)

      And("p ← point(-3, 4, 5)")
      val p = point(x = -3, y = 4, z = 5)

      Then("inv * p = point(-8, 7, 3)")
      val expectedP = point(x = -8, y = 7, z = 3)
      assert(inv * p == expectedP)
    }

    Scenario("Translation does not affect vectors") {
      Given("transform ← translation(5, -3, 2)")
      val transform = translation(5, -3, 2)

      And("v ← vector(-3, 4, 5)")
      val v = vector(x = -3, y = 4, z = 5)

      Then("transform * v = v")
      assert(transform * v == v)
    }
  }

  Feature("Matrix Scaling") {
    Scenario("A scaling matrix applied to a point") {
      Given("transform ← scaling(2, 3, 4)")
      val transform = scaling(2, 3, 4)

      And("p ← point(-4, 6, 8)")
      val p = point(x = -4, y = 6, z = 8)

      Then("transform * p = point(-8, 18, 32)")
      val expectedP = point(-8, 18, 32)
      assert(transform * p == expectedP)
    }

    Scenario("A scaling matrix applied to a vector") {
      Given("transform ← scaling(2, 3, 4)")
      val transform = scaling(2, 3, 4)

      And("v ← vector(-4, 6, 8)")
      val v = vector(x = -4, y = 6, z = 8)

      Then("transform * v = vector(-8, 18, 32)")
      val expectedV = vector(-8, 18, 32)
      assert(transform * v == expectedV)
    }

    Scenario("Multiplying by the inverse of a scaling matrix") {
      Given("transform ← scaling(2, 3, 4)")
      val transform = scaling(2, 3, 4)

      And("inv ← inverse(transform)")
      val inv = inverse(transform)

      And("v ← vector(-4, 6, 8)")
      val v = vector(x = -4, y = 6, z = 8)

      Then("inv * v = vector(-2, 2, 2)")
      val expectedV = vector(-2, 2, 2)
      assert(inv * v == expectedV)
    }

    Scenario("Reflection is scaling by a negative value") {
      Given("transform ← scaling(-1, 1, 1)")
      val transform = scaling(-1, 1, 1)

      And("p ← point(2, 3, 4)")
      val p = point(x = 2, y = 3, z = 4)

      Then("inv * v = vector(-2, 2, 2)")
      val expectedP = vector(-2, 3, 4)
      assert(transform * p == expectedP)
    }
  }

  Feature("Matrix rotation") {
    Scenario("Rotating a point around the x axis") {
      Given("p ← point(0, 1, 0)")
      val p = point(x = 0, y = 1, z = 0)

      And("half_quarter ← rotation_x(π / 4)")
      val half_quarter = rotationX(Pi / 4)

      And("full_quarter ← rotation_x(π / 2)")
      val full_quarter = rotationX(Pi / 2)

      Then("half_quarter * p = point(0, √2/2, √2/2)")
      val expectedPa = point(0, sqrt(2) / 2, sqrt(2) / 2)
      assert(half_quarter * p == expectedPa)

      And("full_quarter * p = point(0, 0, 1)")
      val expectedPb = point(0, 0, 1)
      assert(full_quarter * p == expectedPb)
    }

    Scenario("The inverse of an x-rotation rotates in the opposite direction") {
      Given("p ← point(0, 1, 0)")
      val p = point(x = 0, y = 1, z = 0)

      And("half_quarter ← rotation_x(π / 4)")
      val half_quarter = rotationX(Pi / 4)

      And("inv ← inverse(half_quarter)")
      val inv = inverse(half_quarter)

      Then("inv * p = point(0, √2/2, -√2/2)")
      val expectedP = point(0, sqrt(2) / 2, (-sqrt(2) / 2))
      assert(inv * p == expectedP)
    }

    Scenario("Rotating a point around the y axis") {
      Given("p ← point(0, 0, 1)")
      val p = point(x = 0, y = 0, z = 1)

      And("half_quarter ← rotation_x(π / 4)")
      val half_quarter = rotationY(Pi / 4)

      And("full_quarter ← rotation_x(π / 2)")
      val full_quarter = rotationY(Pi / 2)

      Then("half_quarter * p = point(√2/2, 0, √2/2)")
      val expectedPa = point(sqrt(2) / 2, 0, sqrt(2) / 2)
      assert(half_quarter * p == expectedPa)

      And("full_quarter * p = point(1, 0, 0)")
      val expectedPb = point(1, 0, 0)
      assert(full_quarter * p == expectedPb)
    }

    Scenario("Rotating a point around the z axis") {
      Given("p ← point(0, 1, 0)")
      val p = point(x = 0, y = 1, z = 0)

      And("half_quarter ← rotation_x(π / 4)")
      val half_quarter = rotationZ(Pi / 4)

      And("full_quarter ← rotation_x(π / 2)")
      val full_quarter = rotationZ(Pi / 2)

      Then("half_quarter * p = point(-√2/2, √2/2, 0)")
      val expectedPa = point(-(sqrt(2) / 2), sqrt(2) / 2, 0)
      assert(half_quarter * p == expectedPa)

      And("full_quarter * p = point(-1, 0, 0)")
      val expectedPb = point(-1, 0, 0)
      assert(full_quarter * p == expectedPb)
    }
  }

  Feature("Matrix shearing") {
    Scenario("A shearing transformation moves x in proportion to y") {
      Given("transform ← shearing(1, 0, 0, 0, 0, 0)")
      val transform = shearing(1, 0, 0, 0, 0, 0)

      And("p ← point(2, 3, 4)")
      val p = point(x = 2, y = 3, z = 4)

      Then("transform * p = point(5, 3, 4)")
      val expectedP = point(5, 3, 4)
      assert(transform * p == expectedP)
    }

    Scenario("A shearing transformation moves x in proportion to z") {
      Given("transform ← shearing(0, 1, 0, 0, 0, 0)")
      val transform = shearing(0, 1, 0, 0, 0, 0)

      And("p ← point(2, 3, 4)")
      val p = point(x = 2, y = 3, z = 4)

      Then("transform * p = point(6, 3, 4)")
      val expectedP = point(6, 3, 4)
      assert(transform * p == expectedP)
    }

    Scenario("A shearing transformation moves y in proportion to x") {
      Given("transform ← shearing(0, 0, 1, 0, 0, 0)")
      val transform = shearing(0, 0, 1, 0, 0, 0)

      And("p ← point(2, 3, 4)")
      val p = point(x = 2, y = 3, z = 4)

      Then("transform * p = point(2, 5, 4)")
      val expectedP = point(2, 5, 4)
      assert(transform * p == expectedP)
    }

    Scenario("A shearing transformation moves y in proportion to z") {
      Given("transform ← shearing(0, 0, 0, 1, 0, 0)")
      val transform = shearing(0, 0, 0, 1, 0, 0)

      And("p ← point(2, 3, 4)")
      val p = point(x = 2, y = 3, z = 4)

      Then("transform * p = point(2, 7, 4)")
      val expectedP = point(2, 7, 4)
      assert(transform * p == expectedP)
    }

    Scenario("A shearing transformation moves z in proportion to x") {
      Given("transform ← shearing(0, 0, 0, 0, 1, 0)")
      val transform = shearing(0, 0, 0, 0, 1, 0)

      And("p ← point(2, 3, 4)")
      val p = point(x = 2, y = 3, z = 4)

      Then("transform * p = point(2, 3, 6)")
      val expectedP = point(2, 3, 6)
      assert(transform * p == expectedP)
    }

    Scenario("A shearing transformation moves z in proportion to y") {
      Given("transform ← shearing(0, 0, 0, 0, 0, 1)")
      val transform = shearing(0, 0, 0, 0, 0, 1)

      And("p ← point(2, 3, 4)")
      val p = point(x = 2, y = 3, z = 4)

      Then("transform * p = point(2, 3, 7)")
      val expectedP = point(2, 3, 7)
      assert(transform * p == expectedP)
    }

    Scenario("Individual transformations are applied in sequence") {
      Given("p ← point(1, 0, 1)")
      val p = point(1, 0, 1)

      And("A ← rotation_x(π / 2)")
      val a = rotationX(Pi / 2)

      And("B ← scaling(5, 5, 5)")
      val b = scaling(5, 5, 5)

      And("C ← translation(10, 5, 7)")
      val c = translation(10, 5, 7)

      When("p2 ← A * p")
      val p2 = a * p

      Then("p2 = point(1, -1, 0)")
      assert(p2 == point(1, -1, 0))

      When("p3 ← B * p2")
      val p3 = b * p2

      Then("p3 = point(5, -5, 0)")
      assert(p3 == point(5, -5, 0))

      When("p4 ← C * p3")
      val p4 = c * p3

      Then("p4 = point(15, 0, 7)")
      assert(p4 == point(15, 0, 7))
    }

    Scenario("Chained transformations must be applied in reverse order") {
      Given("p ← point(1, 0, 1)")
      val p = point(1, 0, 1)

      And("A ← rotation_x(π / 2)")
      val a = rotationX(Pi / 2)

      And("B ← scaling(5, 5, 5)")
      val b = scaling(5, 5, 5)

      And("C ← translation(10, 5, 7)")
      val c = translation(10, 5, 7)

      When("T ← C * B * A")
      // the order is _important_!
      // matrix multiplication is associative, but _not_ commutative
      // you must concatenate the transformations in _reverse_ order to get what you want
      val t = c * b * a

      Then("T * p = point(15, 0, 7)")
      assert(t * p == point(15, 0, 7))
    }
  }

  Scenario("The transformation matrix for the default orientation") {
    Given("from ← point(0, 0, 0)")
    val from = point(0, 0, 0)

    And("to ← point(0, 0, -1)")
    val to = point(0, 0, -1)

    And("up ← vector(0, 1, 0)")
    val up = vector(0, 1, 0)

    When("t ← view_transform(from, to, up)")
    val t = viewTransform(from = from, to = to, up = up)

    Then("t = identity_matrix")
    assert(t == identityMatrix())
  }

  Scenario("A view transformation matrix looking in positive z direction") {
    Given("from ← point(0, 0, 0)")
    val from = point(0, 0, 0)

    And("to ← point(0, 0, 1)")
    val to = point(0, 0, 1)

    And("up ← vector(0, 1, 0)")
    val up = vector(0, 1, 0)

    When("t ← view_transform(from, to, up)")
    val t = viewTransform(from = from, to = to, up = up)

    Then("t = scaling(-1, 1, -1)")
    assert(t == scaling(-1, 1, -1))
  }

  Scenario("The view transformation moves the world") {
    Given("from ← point(0, 0, 8)")
    val from = point(0, 0, 8)

    And("to ← point(0, 0, 0)")
    val to = point(0, 0, 0)

    And("up ← vector(0, 1, 0)")
    val up = vector(0, 1, 0)

    When("t ← view_transform(from, to, up)")
    val t = viewTransform(from = from, to = to, up = up)

    Then("t = translation(0, 0, -8)")
    assert(t == translation(0, 0, -8))
  }

  Scenario("An arbitrary view transformation") {
    Given("from ← point(1, 3, 2)")
    val from = point(1, 3, 2)

    And("to ← point(4, -2, 8)")
    val to = point(4, -2, 8)

    And("up ← vector(1, 1, 0)")
    val up = vector(1, 1, 0)

    When("t ← view_transform(from, to, up)")
    val t = viewTransform(from = from, to = to, up = up)

    Then(
      "t is the following 4x4 matrix:\n" +
        "| -0.50709 | 0.50709 |  0.67612 | -2.36643 |\n" +
        "|  0.76772 | 0.60609 |  0.12122 | -2.82843 |\n" +
        "| -0.35857 | 0.59761 | -0.71714 |  0.00000 |\n" +
        "|  0.00000 | 0.00000 |  0.00000 |  1.00000 |"
    )

    val initialValues = Vector[Double](-0.50709, 0.50709, 0.67612, -2.36643, 0.76772, 0.60609, 0.12122, -2.82843,
      -0.35857, 0.59761, -0.71714, 0.00000, 0.00000, 0.00000, 0.00000, 1.00000)
    val b = Matrix(4, initialValues)

    assert(t == b)
  }
}
