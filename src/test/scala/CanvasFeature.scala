package com.craigtreptow.scrayz

import com.craigtreptow.scrayz.CanvasUtils._

class CanvasFeature extends FeatureSpec {
  Feature("Canvas") {
    Scenario("Creating a canvas") {
      Given("c ← canvas(10, 20)")
      val c = Canvas(width = 10, height = 20)

      Then("c.width = 10")
      assert(c.width == 10)

      And("c.height = 20")
      assert(c.height == 20)

      And("every pixel of c is color(0, 0, 0)")
      val height = 3
      val width = 5
      val black = Color(red = 0, green = 0, blue = 0)
      // zero based
      // c(width)(height)

      for {
        y <- 0 until height
        x <- 0 until width
      } assert(pixelAt(x, y, c) == black)
    }
  }

  Scenario("Writing pixels to a canvas") {
    Given("c ← canvas(10, 20)")
    val c = Canvas(width = 10, height = 20)

    And("red ← color(1, 0, 0)")
    val red = Color(red = 1, blue = 0, green = 0)

    When("write_pixel(c, 2, 3, red)")
    val updatedC = writePixelAt(newColor = red, x = 2, y = 3, canvas = c)

    Then("pixel_at(c, 2, 3) = red")
    assert(pixelAt(canvas = updatedC, x = 2, y = 3) == red)
  }

  Scenario("Constructing the PPM header") {
    Given("c ← canvas(5, 3)")
    val c = Canvas(width = 5, height = 3)

    When("ppm ← canvas_to_ppm(c)")
    val lines = canvasToLines(c)

    Then("lines 1-3 of ppm are")
    assert(lines(0) == "P3")
    assert(lines(1) == "\n")
    assert(lines(2) == "5 3")
    assert(lines(3) == "\n")
    assert(lines(4) == "255")
  }

  Scenario("Constructing the PPM pixel data") {
    Given("c ← canvas(5, 3)")
    val canv = Canvas(width = 5, height = 3)

    When("c1 ← color(1.5, 0, 0)")
    val c1 = Color(red = 1.5, green = 0, blue = 0)

    And("c2 ← color(0, 0.5, 0)")
    val c2 = Color(red = 0, green = 0.5, blue = 0)

    And("c3 ← color(-0.5, 0, 1)")
    val c3 = Color(red = -0.5, green = 0, blue = 1)

    When("write_pixel(c, 0, 0, c1)")
    val canv1 = writePixelAt(newColor = c1, x = 0, y = 0, canvas = canv)

    And("write_pixel(c, 2, 1, c2)")
    val canv2 = writePixelAt(newColor = c2, x = 2, y = 1, canvas = canv1)

    When("write_pixel(c, 4, 2, c3)")
    val canv3 = writePixelAt(newColor = c3, x = 4, y = 2, canvas = canv2)

    And("ppm ← canvas_to_ppm(c)")
    val lines = canvasToLines(canv3)
    // Utils.printCanvas(canv3)

    assert(clamp(0) == 0)
    assert(clamp(1.0) == 255)
    assert(clamp(1.1) == 255)
    assert(clamp(-0.1) == 0)

    Then("lines 4-6 of ppm are")
    assert(lines(6) == "255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ")
    assert(lines(7) == "0 0 0 0 0 0 0 128 0 0 0 0 0 0 0 ")
    assert(lines(8) == "0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 ")
    assert(lines(9) == "\n")
  }

  Scenario("PPM files are terminated by a newline character") {
    Given("c ← canvas(5, 3)")
    val canv = Canvas(width = 5, height = 3)

    When("ppm ← canvas_to_ppm")
    val lines = canvasToLines(canv)
    // Utils.printCanvas(canv3)

    Then("ppm ends with a newline character")
    assert(lines.last == "\n")
  }

  // Does not matter on macOS using the Preview app
  // or on Windows using GIMP
//  Scenario("Splitting long lines in PPM files") {
//    Given("c ← canvas(10, 2)")
//    When("c ← every pixel of c is set to color(1, 0.8, 0.6)")
//    val c = Color(red = 1, green = 0.8, blue = 0.6)
//    val canv = Canvas(width = 10, height = 2, color = c)
//    Utils.printCanvas(canv)
//
//    And("ppm ← canvas_to_ppm(c)")
//    val lines = canvasToPPM(canv)
//    println("Line")
//    println(lines(3))
//
//    Then("lines 4-7 of ppm are")
//    assert(
//      lines(
//        3
//      ) == "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204"
//    )
//    // assert(lines(4) == "153 255 204 153 255 204 153 255 204 153 255 204 153")
//    // assert(lines(5) == "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204")
//    // assert(lines(6) == "153 255 204 153 255 204 153 255 204 153 255 204 153")
//  }

}
