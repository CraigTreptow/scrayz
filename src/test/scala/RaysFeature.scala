package com.craigtreptow.scrayz

import Builders.{point, vector}
import RayUtils._
import MatrixUtils._

class RaysFeature extends FeatureSpec {
  Scenario("Creating and querying a ray") {
    Given("origin ← point(1, 2, 3)")
    val origin = point(x = 1, y = 2, z = 3)

    And("direction ← vector(4, 5, 6)")
    val direction = vector(x = 4, y = 5, z = 6)

    When("r ← ray(origin, direction)")
    val r = Ray(origin = origin, direction = direction)

    Then("r.origin = origin")
    assert(r.origin == origin)

    And("r.direction = direction")
    assert(r.direction == direction)
  }

  Scenario("Computing a point from a distance") {
    Given("r ← ray(point(2, 3, 4), vector(1, 0, 0")
    val p = point(2, 3, 4)
    val v = vector(1, 0, 0)
    val r = Ray(origin = p, direction = v)

    Then("position(r, 0) = point(2, 3, 4)")
    val pos0 = position(r, 0)
    assert(pos0 == point(2, 3, 4))

    And("position(r, 1) = point(3, 3, 4)")
    val pos1 = position(r, 1)
    assert(pos1 == point(3, 3, 4))

    And("position(r, -1) = point(1, 3, 4)")
    val pos2 = position(r, -1)
    assert(pos2 == point(1, 3, 4))

    And("position(r, 2.5) = point(4.5, 3, 4)")
    val pos3 = position(r, 2.5)
    assert(pos3 == point(4.5, 3, 4))
  }

  Scenario("Translating a ray") {
    Given("r ← ray(point(1, 2, 3), vector(0, 1, 0")
    val p = point(1, 2, 3)
    val v = vector(0, 1, 0)
    val r = Ray(origin = p, direction = v)

    And("m ← translation(3, 4, 5)")
    val m = translation(3, 4, 5)

    When("r2 ← transform(r, m)")
    val r2 = transform(r, m)

    Then("r2.origin = point(4, 6, 8)")
    assert(r2.origin == point(4, 6, 8))

    And("r2.direction = vector(0, 1, 0)")
    assert(r2.direction == vector(0, 1, 0))
  }

  Scenario("Scaling a ray") {
    Given("r ← ray(point(1, 2, 3), vector(0, 1, 0")
    val p = point(1, 2, 3)
    val v = vector(0, 1, 0)
    val r = Ray(origin = p, direction = v)

    And("m ← scaling(2, 3, 4)")
    val m = scaling(2, 3, 4)

    When("r2 ← transform(r, m)")
    val r2 = transform(r, m)

    Then("r2.origin = point(2, 6, 12)")
    assert(r2.origin == point(2, 6, 12))

    And("r2.direction = vector(0, 3, 0)")
    assert(r2.direction == vector(0, 3, 0))
  }
}
