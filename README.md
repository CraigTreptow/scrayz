# Scrayz

A ray tracer built with Scala using the book ["The Ray Tracer Challenge"](http://raytracerchallenge.com/)

## ToDo

- [x] Add tests from book source code
- [x] Chapter 1
- [x] Chapter 2
- [x] Chapter 3
- [x] Chapter 4
- [x] Chapter 5
- [X] Chapter 6
- [X] Chapter 7
- [ ] Chapter 8
- [ ] Chapter 9
- [ ] Chapter 10
- [ ] Chapter 11
- [ ] Chapter 12
- [ ] Chapter 13
- [ ] Chapter 14
- [ ] Chapter 15
- [ ] Chapter 16
- [ ] Chapter 17

## Notes

### Coordinate system

This book uses the _left-handed_ coordinate system.  So, using your left hand, 
the `x` axis (thumb) points to the right, the `y` axis (fingers) point up.  If you
curl your fingers, they curl *away* from you, which is the direction of `z`.

The canvas is accessed like `canvas(width)(height)`, where width an height are zero-based indices.

```
width = 5, height = 3

    X
        0    -    -    -    >
Y  0
   |            (1, 2) 
   V                      (2,4)
```

### Radians

> A full circle (360 degrees) consists of 2π radians, which means a half circle (180 degrees) is π radians, and a quarter circle (90 degrees) is π/2 radians. If you’re not used to thinking in terms of radians, it may be helpful to write a function to convert them from degrees.  That formula is:

π radians = 180 degrees
1 radian = 180 degrees / π
degrees = radians * ( 180 / π )


### Shading

Four vectors are needed to (approximately) shade:

- E is the _eye vector_, pointing from a point P, to the origin of the ray (usually where the eye exists that is looking at the scene)
- L is the _light vector_, pointing from a point P to the position of the light source
- N is the _surface normal_, a vector that is perpendicular to the surface at P
- R is the _reflection vector_, pointing in the direction that incoming light would bounce, or reflect

![vectors image from the book](./images/vector.png "vectors from the book")

### World Space vs Object Space

World Origin = point(0, 0, 0)

From the book:

> Another way to think about transformation matrices is to think of them as converting points between two different coordinate systems. At the scene level, everything is in world space coordinates, relative to the overall world. But at the object level, everything is in object space coordinates, relative to the object itself.

> Multiplying a point in object space by a transformation matrix converts that point to world space—scaling it, translating, rotating it, or whatever. Multiplying a point in world space by the inverse of the transformation matrix converts that point back to object space.

> Want to intersect a ray in world space with a sphere in object space? Just convert the ray’s origin and direction to that same object space, and you’re golden.

## Thoughts

### Numbers

I fought a surprising number of issues with `Double` accuracy and comparing them.  I did get the author's suggestion to work everywhere I need it in the `Utils` module.

### Data Modeling

It still seems strange to use `case class` so much to model data.  I was also forced to implement methods in some classes, which all feels very OOP to me.  I'm sure there is more that I don't understand and a better way may become clear as time goes on and I learn more.

I also created a `case class` called `Tuple`, but do think I should have `Point` and `Vector` types instead, but of course `Vector` is used.  I got away with `Tuple`, because the built-in names start with `Tuple2`, so there was no conflict.  In any case, this feels messy and I would like to find another way.

After updating to Scala3 and learning about extension methods, I tried those.  However, nothing I did could get arond the errors regarding how say the `+` method was already defined in `Color`, so couldn't be defined in `Tuple` as well.  This was in spite of the fact that I had the return type specified.  The only way I got this to work, was to use the _Functional Objects_ approach.

This was to define the behaviors(methods) in the _same type_ as the data.  They implement the behavior as pure functions by using `this.copy` so nothing is mutated.

I don't like how OOP this now feels by doing things like `v.isVector`.

### Types

In chapter 2, I struggled for a *long* time generating the lines for the PPM.  I kept fighting the types.  Some things will not return the type I expected.  Sometimes you get some sort of indexed type
and need to convert to what you want (e.g. Vector).  

One thing I learned is that a `for` comprehension will default to the type of the first
thing it iterates over.  In my case, using `until` and the height of the canvas (Int) 
resulted in one of these iterable index types, rather than a `Vector` or `Array` or anything
else I could have dealt with better.

In chapter 3, I still fought strange things with types.  For instance:

`toVector` is required here.
```
val m = i.orderedValues
val mm = m.updated(0, x)
val mmm = mm.updated(5, y)
val mmmm = mmm.updated(10, z)
val vals = mmmm.toVector
```

`toVector` was flagged as redundant when I cleaned up the above code to this
```
 val vals = i.orderedValues.updated(0, x).updated(5, y).updated(10, z)
```

I don't understand why sometimes it is a `Vector` and other times it is not.

Somewhere, the type must get set and inferred correctly in the second version and not in the first.

### Testing

I'm using Feature style tests since the publisher provided them, 
and I thought it would save me some effort in typing, as well as 
in tracking down bugs later.

